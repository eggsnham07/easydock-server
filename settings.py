config = {
    "bindings": {
        "left-left": "/usr/bin/thonny",
        "left-right": "lxterminal",
        "middle": "flatpak run com.thebrokenrail.MCPIReborn",
        "right-left": "chocolate-doom-setup",
        "right-right": "code"  
    }
}
