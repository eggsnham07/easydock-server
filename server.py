from subprocess import Popen
from flask import Flask
from settings import config
import os

app = Flask(__name__)
binds = config["bindings"]

def run(action:str):
    devnull = open(os.devnull, "wb")
    Popen(["nohup", binds[action]], stdout=devnull, stderr=devnull)

@app.route("/buttons/left_left")
def left_left():
    run("left-left")
    return "Running"

@app.route("/buttons/left_right")
def left_right():
    run("left-right")
    return "Running"

@app.route("/buttons/middle")
def middle():
    run("middle")
    return "Running"

@app.route("/buttons/right_left")
def right_left():
    print("Recived on right_left")
    run("right-left")
    return "Running"

@app.route("/buttons/right_right")
def right_right():
    print("Recived on /buttons/right_right")
    run("right-right")
    return "Running"

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=2516)
